﻿using Nop.Core.Domain.FAQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.FAQ
{
    public partial class FAQMap : NopEntityTypeConfiguration<FAQuestion>
    {
        public FAQMap()
        {
            this.ToTable("FAQuestion");
            this.HasKey(o => o.Id);
        }
    }
}
