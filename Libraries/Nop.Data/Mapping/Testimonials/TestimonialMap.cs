﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Testimonials;

namespace Nop.Data.Mapping.Testimonials
{
    public partial class TestimonialMap : NopEntityTypeConfiguration<Testimonial>
    {
        public TestimonialMap()
        {
            this.ToTable("Testimonial");
            this.HasKey(o => o.Id);
        }
    }
}
