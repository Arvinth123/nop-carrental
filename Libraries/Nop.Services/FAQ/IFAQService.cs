﻿using Nop.Core.Domain.FAQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.FAQ
{
    public partial interface IFAQService
    {
        /// <summary>
        /// Gets all FAQuestions
        /// </summary>
        /// <returns>FAQuestions</returns>
        IList<FAQuestion> GetAllFAQuestions();

        /// <summary>
        /// Gets FAQuestion by identifier
        /// </summary>
        /// <param name="FAQuestionIds">FAQuestion identifiers</param>
        /// <returns>FAQuestion</returns>
        IList<FAQuestion> GetFAQuestionByIds(int[] FAQuestionIds);

        /// <summary>
        /// Delete FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        void DeleteFAQuestions(IList<FAQuestion> FAQuestions);

        /// <summary>
        /// Update FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        void UpdateFAQuestions(IList<FAQuestion> FAQuestions);

        /// <summary>
        /// Inserts a FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        void InsertFAQuestion(FAQuestion FAQuestion);

        /// <summary>
        /// Gets FAQuestion
        /// </summary>
        /// <param name="FAQuestionId">FAQuestion identifier</param>
        /// <returns>Testimonial</returns>
        FAQuestion GetFAQuestionById(int FAQuestionId);

        /// <summary>
        /// Updates the FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        void UpdateFAQuestion(FAQuestion FAQuestion);

        /// <summary>
        /// Delete a FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        void DeleteFAQuestion(FAQuestion FAQuestion);

        /// <summary>
        /// Gets FAQCategory
        /// </summary>
        /// <returns>FAQCategory</returns>
        IList<FAQCategory> GetAllFAQCategorypub();

        /// <summary>
        /// Gets FAQCategory
        /// </summary>
        /// <returns>FAQCategory</returns>
        IList<FAQCategory> GetAllFAQCategory();

        /// <summary>
        /// Gets FAQCategory by id
        /// </summary>
        /// <returns>FAQCategory name</returns>
        string GetFAQCategorynamebyid(int FAQCategoryId);

        /// <summary>
        /// Delete FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        void DeleteFAQCategories(IList<FAQCategory> FAQCategorys);

        /// <summary>
        /// Update FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        void UpdateFAQCategorys(IList<FAQCategory> FAQCategorys);

        /// <summary>
        /// Gets FAQCategory by identifier
        /// </summary>
        /// <param name="FAQCategoryIds">FAQCategory identifiers</param>
        /// <returns>FAQCategory</returns>
        IList<FAQCategory> GetFAQCategoryByIds(int[] FAQCategoryIds);

        /// <summary>
        /// Inserts a FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        void InsertFAQCategory(FAQCategory FAQCategory);

        /// <summary>
        /// Gets FAQCategory
        /// </summary>
        /// <param name="FAQCategoryId">FAQCategory identifier</param>
        /// <returns>FAQCategory</returns>
        FAQCategory GetFAQCategoryById(int FAQCategoryId);

        /// <summary>
        /// Updates the FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        void UpdateFAQCategory(FAQCategory FAQCategory);

        /// <summary>
        /// Delete a FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        void DeleteFAQCategory(FAQCategory FAQCategory);
    }
}
