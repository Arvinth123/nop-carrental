﻿using Nop.Core.Data;
using Nop.Core.Domain.FAQ;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.FAQ
{
    public partial class FAQService : IFAQService
    {
        private readonly IRepository<FAQuestion> _FAQuestionRepository;
        private readonly IRepository<FAQCategory> _FAQCategoryRepository;
        private readonly IEventPublisher _eventPublisher;

        public FAQService(
            IRepository<FAQuestion> FAQuestionRepository,
            IEventPublisher eventPublisher,
            IRepository<FAQCategory> FAQCategoryRepository)
        
        {
            this._FAQuestionRepository = FAQuestionRepository;
            this._eventPublisher = eventPublisher;
            this._FAQCategoryRepository = FAQCategoryRepository;
        }

        /// <summary>
        /// Gets all FAQuestions
        /// </summary>
        /// <returns>FAQuestions</returns>
        public virtual IList<FAQuestion> GetAllFAQuestions()
        {
            var query = from t in _FAQuestionRepository.Table
                        where !t.Deleted
                        orderby t.DisplayOrder
                        select t;
            return query.ToList();
        }

        /// <summary>
        /// Gets FAQuestion by identifier
        /// </summary>
        /// <param name="FAQuestionIds">FAQuestion identifiers</param>
        /// <returns>FAQuestion</returns>
        public virtual IList<FAQuestion> GetFAQuestionByIds(int[] FAQuestionIds)
        {
            if (FAQuestionIds == null || FAQuestionIds.Length == 0)
                return new List<FAQuestion>();

            var query = from t in _FAQuestionRepository.Table
                        where FAQuestionIds.Contains(t.Id) && !t.Deleted
                        select t;
            var FAQuestions = query.ToList();
            //sort by passed identifiers
            var sortedProducts = new List<FAQuestion>();
            foreach (var id in FAQuestionIds)
            {
                var product = FAQuestions.Find(x => x.Id == id);
                if (product != null)
                    sortedProducts.Add(product);
            }
            return sortedProducts;
        }

        /// <summary>
        /// Delete FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        public virtual void DeleteFAQuestions(IList<FAQuestion> FAQuestions)
        {
            if (FAQuestions == null)
                throw new ArgumentNullException(nameof(FAQuestions));

            foreach (var FAQuestion in FAQuestions)
            {
                FAQuestion.Deleted = true;
            }

            //delete FAQuestion
            UpdateFAQuestions(FAQuestions);

            foreach (var FAQuestion in FAQuestions)
            {
                //event notification
                _eventPublisher.EntityDeleted(FAQuestion);
            }
        }

        /// <summary>
        /// Update FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        public virtual void UpdateFAQuestions(IList<FAQuestion> FAQuestions)
        {
            if (FAQuestions == null)
                throw new ArgumentNullException(nameof(FAQuestions));

            //update
            _FAQuestionRepository.Update(FAQuestions);

            //event notification
            foreach (var FAQuestion in FAQuestions)
            {
                _eventPublisher.EntityUpdated(FAQuestion);
            }
        }

        /// <summary>
        /// Inserts a FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        public virtual void InsertFAQuestion(FAQuestion FAQuestion)
        {
            if (FAQuestion == null)
                throw new ArgumentNullException(nameof(FAQuestion));

            //insert
            _FAQuestionRepository.Insert(FAQuestion);

            //event notification
            _eventPublisher.EntityInserted(FAQuestion);
        }

        /// <summary>
        /// Gets FAQuestion
        /// </summary>
        /// <param name="FAQuestionId">FAQuestion identifier</param>
        /// <returns>FAQuestion</returns>
        public virtual FAQuestion GetFAQuestionById(int FAQuestionId)
        {
            if (FAQuestionId == 0)
                return null;

            var query = _FAQuestionRepository.GetById(FAQuestionId);

            return query;
        }

        /// <summary>
        /// Updates the FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        public virtual void UpdateFAQuestion(FAQuestion FAQuestion)
        {
            if (FAQuestion == null)
                throw new ArgumentNullException(nameof(FAQuestion));

            //update
            _FAQuestionRepository.Update(FAQuestion);

            //event notification
            _eventPublisher.EntityUpdated(FAQuestion);
        }

        /// <summary>
        /// Delete a FAQuestion
        /// </summary>
        /// <param name="FAQuestion">FAQuestion</param>
        public virtual void DeleteFAQuestion(FAQuestion FAQuestion)
        {
            if (FAQuestion == null)
                throw new ArgumentNullException(nameof(FAQuestion));

            FAQuestion.Deleted = true;
            //delete FAQuestion
            UpdateFAQuestion(FAQuestion);

            //event notification
            _eventPublisher.EntityDeleted(FAQuestion);
        }

        /// <summary>
        /// Gets FAQCategory
        /// </summary>
        /// <returns>FAQCategory</returns>
        public virtual IList<FAQCategory> GetAllFAQCategorypub()
        {
            var query = from t in _FAQCategoryRepository.Table
                        where !t.Deleted && t.Published
                        orderby t.DisplayOrder
                        select t;
            return query.ToList();
        }

        /// <summary>
        /// Gets FAQCategory
        /// </summary>
        /// <returns>FAQCategory</returns>
        public virtual IList<FAQCategory> GetAllFAQCategory()
        {
            var query = from t in _FAQCategoryRepository.Table
                        where !t.Deleted
                        orderby t.DisplayOrder
                        select t;
            return query.ToList();
        }

        /// <summary>
        /// Gets FAQCategory by id
        /// </summary>
        /// <returns>FAQCategory name</returns>
        public virtual string GetFAQCategorynamebyid(int FAQCategoryId)
        {
            var query = from t in _FAQCategoryRepository.Table
                        where !t.Deleted && t.Id == FAQCategoryId
                        orderby t.DisplayOrder
                        select t.CategoryName;
            return query.FirstOrDefault();
        }

        /// <summary>
        /// Delete FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        public virtual void DeleteFAQCategories(IList<FAQCategory> FAQCategorys)
        {
            if (FAQCategorys == null)
                throw new ArgumentNullException(nameof(FAQCategorys));

            foreach (var FAQCategory in FAQCategorys)
            {
                FAQCategory.Deleted = true;
            }

            //delete FAQCategory
            UpdateFAQCategorys(FAQCategorys);

            foreach (var FAQCategory in FAQCategorys)
            {
                //event notification
                _eventPublisher.EntityDeleted(FAQCategory);
            }
        }

        /// <summary>
        /// Update FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        public virtual void UpdateFAQCategorys(IList<FAQCategory> FAQCategorys)
        {
            if (FAQCategorys == null)
                throw new ArgumentNullException(nameof(FAQCategorys));

            //update
            _FAQCategoryRepository.Update(FAQCategorys);

            //event notification
            foreach (var FAQCategory in FAQCategorys)
            {
                _eventPublisher.EntityUpdated(FAQCategory);
            }
        }

        /// <summary>
        /// Gets FAQCategory by identifier
        /// </summary>
        /// <param name="FAQCategoryIds">FAQCategory identifiers</param>
        /// <returns>FAQCategory</returns>
        public virtual IList<FAQCategory> GetFAQCategoryByIds(int[] FAQCategoryIds)
        {
            if (FAQCategoryIds == null || FAQCategoryIds.Length == 0)
                return new List<FAQCategory>();

            var query = from t in _FAQCategoryRepository.Table
                        where FAQCategoryIds.Contains(t.Id) && !t.Deleted
                        select t;
            var FAQCategorys = query.ToList();
            //sort by passed identifiers
            var sortedProducts = new List<FAQCategory>();
            foreach (var id in FAQCategoryIds)
            {
                var product = FAQCategorys.Find(x => x.Id == id);
                if (product != null)
                    sortedProducts.Add(product);
            }
            return sortedProducts;
        }

        /// <summary>
        /// Inserts a FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        public virtual void InsertFAQCategory(FAQCategory FAQCategory)
        {
            if (FAQCategory == null)
                throw new ArgumentNullException(nameof(FAQCategory));

            //insert
            _FAQCategoryRepository.Insert(FAQCategory);

            //event notification
            _eventPublisher.EntityInserted(FAQCategory);
        }

        /// <summary>
        /// Gets FAQCategory
        /// </summary>
        /// <param name="FAQCategoryId">FAQCategory identifier</param>
        /// <returns>FAQCategory</returns>
        public virtual FAQCategory GetFAQCategoryById(int FAQCategoryId)
        {
            if (FAQCategoryId == 0)
                return null;

            var query = _FAQCategoryRepository.GetById(FAQCategoryId);

            return query;
        }

        /// <summary>
        /// Updates the FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        public virtual void UpdateFAQCategory(FAQCategory FAQCategory)
        {
            if (FAQCategory == null)
                throw new ArgumentNullException(nameof(FAQCategory));

            //update
            _FAQCategoryRepository.Update(FAQCategory);

            //event notification
            _eventPublisher.EntityUpdated(FAQCategory);
        }

        /// <summary>
        /// Delete a FAQCategory
        /// </summary>
        /// <param name="FAQCategory">FAQCategory</param>
        public virtual void DeleteFAQCategory(FAQCategory FAQCategory)
        {
            if (FAQCategory == null)
                throw new ArgumentNullException(nameof(FAQCategory));

            FAQCategory.Deleted = true;
            //delete FAQuestion
            UpdateFAQCategory(FAQCategory);

            //event notification
            _eventPublisher.EntityDeleted(FAQCategory);
        }

    }
}
