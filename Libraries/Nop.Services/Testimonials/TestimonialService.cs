﻿using Nop.Core.Data;
using Nop.Core.Domain.Testimonials;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Testimonials
{
    public partial class TestimonialService : ITestimonialService
    {

        private readonly IRepository<Testimonial> _testimonialRepository;
        private readonly IEventPublisher _eventPublisher;

        public TestimonialService(
            IRepository<Testimonial> testimonialRepository,
            IEventPublisher eventPublisher)

        {
            this._testimonialRepository = testimonialRepository;
            this._eventPublisher = eventPublisher;
        }

        /// <summary>
        /// Gets all Testimonials
        /// </summary>
        /// <returns>Testimonials</returns>
        public virtual IList<Testimonial> GetAllTestimonials()
        {
                var query = from t in _testimonialRepository.Table
                            where !t.Deleted
                            orderby t.CreatedDate descending
                            select t;
                return query.ToList();
        }

        /// <summary>
        /// Gets Testimonial by identifier
        /// </summary>
        /// <param name="TestimonialIds">Testimonial identifiers</param>
        /// <returns>Testimonials</returns>
        public virtual IList<Testimonial> GetTestimonialsByIds(int[] TestimonialIds)
        {
            if (TestimonialIds == null || TestimonialIds.Length == 0)
                return new List<Testimonial>();

            var query = from t in _testimonialRepository.Table
                        where TestimonialIds.Contains(t.Id) && !t.Deleted
                        select t;
            var Testimonials = query.ToList();
            //sort by passed identifiers
            var sortedProducts = new List<Testimonial>();
            foreach (var id in TestimonialIds)
            {
                var product = Testimonials.Find(x => x.Id == id);
                if (product != null)
                    sortedProducts.Add(product);
            }
            return sortedProducts;
        }

        /// <summary>
        /// Delete Testimonials
        /// </summary>
        /// <param name="Testimonials">Testimonials</param>
        public virtual void DeleteTestimonials(IList<Testimonial> Testimonials)
        {
            if (Testimonials == null)
                throw new ArgumentNullException(nameof(Testimonials));

            foreach (var Testimonial in Testimonials)
            {
                Testimonial.Deleted = true;
            }

            //delete Testimonial
            UpdateTestimonials(Testimonials);

            foreach (var Testimonial in Testimonials)
            {
                //event notification
                _eventPublisher.EntityDeleted(Testimonial);
            }
        }

        /// <summary>
        /// Update Testimonials
        /// </summary>
        /// <param name="Testimonials">Testimonials</param>
        public virtual void UpdateTestimonials(IList<Testimonial> Testimonials)
        {
            if (Testimonials == null)
                throw new ArgumentNullException(nameof(Testimonials));

            //update
            _testimonialRepository.Update(Testimonials);

            //event notification
            foreach (var Testimonial in Testimonials)
            {
                _eventPublisher.EntityUpdated(Testimonial);
            }
        }

        /// <summary>
        /// Gets Testimonial
        /// </summary>
        /// <param name="TestimonialId">Testimonial identifier</param>
        /// <returns>Testimonial</returns>
        public virtual Testimonial GetTestimonialById(int TestimonialId)
        {
            if (TestimonialId == 0)
                return null;

            var query = _testimonialRepository.GetById(TestimonialId);

            return query;
        }


        /// <summary>
        /// Updates the Testimonial
        /// </summary>
        /// <param name="Testimonial">Testimonial</param>
        public virtual void UpdateTestimonial(Testimonial Testimonial)
        {
            if (Testimonial == null)
                throw new ArgumentNullException(nameof(Testimonial));

            //update
            _testimonialRepository.Update(Testimonial);

            //event notification
            _eventPublisher.EntityUpdated(Testimonial);
        }

        /// <summary>
        /// Inserts a Testimonial
        /// </summary>
        /// <param name="Testimonial">Testimonial</param>
        public virtual void InsertTestimonial(Testimonial Testimonial)
        {
            if (Testimonial == null)
                throw new ArgumentNullException(nameof(Testimonial));

            //insert
            _testimonialRepository.Insert(Testimonial);
            
            //event notification
            _eventPublisher.EntityInserted(Testimonial);
        }

        /// <summary>
        /// Delete a Testimonial
        /// </summary>
        /// <param name="Testimonial">Testimonial</param>
        public virtual void DeleteTestimonial(Testimonial Testimonial)
        {
            if (Testimonial == null)
                throw new ArgumentNullException(nameof(Testimonial));

            Testimonial.Deleted = true;
            //delete product
            UpdateTestimonial(Testimonial);

            //event notification
            _eventPublisher.EntityDeleted(Testimonial);
        }

        /// <summary>
        /// Gets all Testimonial in testimonial page
        /// </summary>
        /// <returns>Testimonial</returns>
        public virtual IList<Testimonial> GetAllTestimonialsPage()
        {
            var query = from t in _testimonialRepository.Table
                        where !t.Deleted && t.IsPublished && t.TestimonialPage
                        orderby t.DisplayOrder , t.CreatedDate descending
                        select t;
            return query.ToList();
        }

        /// <summary>
        /// Gets all Testimonial in home page
        /// </summary>
        /// <returns>Testimonial</returns>
        public virtual IList<Testimonial> GetAllTestimonialsHomePage()
        {
            var query = from t in _testimonialRepository.Table
                        where !t.Deleted && t.IsPublished && t.HomePage
                        orderby t.DisplayOrder, t.CreatedDate descending
                        select t;
            return query.ToList();
        }
    }
}
