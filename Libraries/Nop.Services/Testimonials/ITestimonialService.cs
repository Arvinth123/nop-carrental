﻿using Nop.Core.Domain.Testimonials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Testimonials
{
    public partial interface ITestimonialService
    {
        /// <summary>
        /// Gets all Testimonial
        /// </summary>
        /// <returns>Testimonial</returns>
        IList<Testimonial> GetAllTestimonials();

        /// <summary>
        /// Gets Testimonial by identifier
        /// </summary>
        /// <param name="TestimonialIds">Testimonial identifiers</param>
        /// <returns>Testimonials</returns>
        IList<Testimonial> GetTestimonialsByIds(int[] TestimonialIds);

        /// <summary>
        /// Delete Testimonials
        /// </summary>
        /// <param name="Testimonials">Testimonials</param>
        void DeleteTestimonials(IList<Testimonial> Testimonials);

        /// <summary>
        /// Update Testimonials
        /// </summary>
        /// <param name="Testimonials">Testimonials</param>
        void UpdateTestimonials(IList<Testimonial> Testimonials);

        /// <summary>
        /// Gets Testimonial
        /// </summary>
        /// <param name="TestimonialId">Testimonial identifier</param>
        /// <returns>Testimonial</returns>
        Testimonial GetTestimonialById(int TestimonialId);

        /// <summary>
        /// Updates the Testimonial
        /// </summary>
        /// <param name="Testimonial">Testimonial</param>
        void UpdateTestimonial(Testimonial Testimonial);

        /// <summary>
        /// Inserts a Testimonial
        /// </summary>
        /// <param name="Testimonial">Testimonial</param>
        void InsertTestimonial(Testimonial Testimonial);

        /// <summary>
        /// Delete a Testimonial
        /// </summary>
        /// <param name="Testimonial">Testimonial</param>
        void DeleteTestimonial(Testimonial Testimonial);

        /// <summary>
        /// Gets all Testimonial in testimonial page
        /// </summary>
        /// <returns>Testimonial</returns>
        IList<Testimonial> GetAllTestimonialsPage();

        /// <summary>
        /// Gets all Testimonial in home page
        /// </summary>
        /// <returns>Testimonial</returns>
        IList<Testimonial> GetAllTestimonialsHomePage();
    }
}
