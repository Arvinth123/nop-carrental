﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.FAQ
{
    public partial class FAQuestion : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the CategoryId
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the Question
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the Answer
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the DisplayOrder
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the FAQCategory is Published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the FAQCategory is deleted
        /// </summary>
        public bool Deleted { get; set; }
    }
}
