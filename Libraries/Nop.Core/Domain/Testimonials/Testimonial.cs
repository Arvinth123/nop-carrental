﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Testimonials
{
    public partial class Testimonial : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Customer identifier
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the CustomerName
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the Rating
        /// </summary>
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets the Review
        /// </summary>
        public string Review { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the testimonial is Published
        /// </summary>
        public bool IsPublished { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the testimonial shows in HomePage
        /// </summary>
        public bool HomePage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the testimonial shows in TestimonialPage
        /// </summary>
        public bool TestimonialPage { get; set; }

        /// <summary>
        /// Gets or sets the DisplayOrder
        /// </summary>
        public int? DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the testimonial is deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the Avatar
        /// </summary>
        public string Avatar { get; set; }
    }
}
