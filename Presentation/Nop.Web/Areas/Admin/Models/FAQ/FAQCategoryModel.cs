﻿using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.FAQ
{
    public class FAQCategoryModel : BaseNopEntityModel, ILocalizedModel<FAQCategoryLocalizedModel>
    {
        public FAQCategoryModel()
        {
            Locales = new List<FAQCategoryLocalizedModel>();
        }
        [Required(ErrorMessage = "Please provide the Category Name.")]
        [NopResourceDisplayName("admin.contentmanagement.faqCategory.fields.CategoryName")]
        public string CategoryName { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faqCategory.fields.date")]
        public DateTime CreatedDate { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faqCategory.fields.display")]
        public int DisplayOrder { get; set; }
         
        public bool Deleted { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faqCategory.fields.published")]
        public bool Published { get; set; }

        public IList<FAQCategoryLocalizedModel> Locales { get; set; }
    }

    public partial class FAQCategoryLocalizedModel : ILocalizedModelLocal
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faqCategory.fields.CategoryName")]
        public string CategoryName { get; set; }
    }
}
