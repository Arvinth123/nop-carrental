﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.FAQ
{
    public partial class FAQModel : BaseNopEntityModel, ILocalizedModel<FAQLocalizedModel>
    {
        public FAQModel()
        {
            Locales = new List<FAQLocalizedModel>();
            Categories = new List<SelectListItem>();
        }

        [Required(ErrorMessage = "Please select a Category.")]
        [NopResourceDisplayName("admin.contentmanagement.faq.fields.category")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Please provide a question.")]
        [NopResourceDisplayName("admin.contentmanagement.faq.fields.question")]
        public string Question { get; set; }

        [Required(ErrorMessage = "Please provide a suitable answer for the above question.")]
        [NopResourceDisplayName("admin.contentmanagement.faq.fields.answer")]
        public string Answer { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faq.fields.date")]
        public DateTime CreatedDate { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faq.fields.display")]
        public int DisplayOrder { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faq.fields.published")]
        public bool Published { get; set; }
        
        public bool Deleted { get; set; }

        public string CategoryName { get; set; }

        public IList<FAQLocalizedModel> Locales { get; set; }

        public IList<SelectListItem> Categories { get; set; }

    }
    public partial class FAQLocalizedModel : ILocalizedModelLocal
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faq.fields.question")]
        public string Question { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.faq.fields.answer")]
        public string Answer { get; set; }
    }
}
