﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Testimonials
{
    public class TestimonialModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.id")]
        public int? CustomerId { get; set; }

        [Required(ErrorMessage = "Please provide a Name")]
        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.name")]
        public string CustomerName { get; set; }

        [Range(1, 5, ErrorMessage = "Enter number between 1 to 5")]
        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.rating")]
        public int Rating { get; set; }

        [Required(ErrorMessage = "Please provide Review")]
        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.review")]
        public string Review { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.date")]
        public DateTime CreatedDate { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.published")]
        public bool IsPublished { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.homepage")]
        public bool HomePage { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.testimonialpage")]
        public bool TestimonialPage { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.display")]
        public int? DisplayOrder { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.deleted")]
        public bool Deleted { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.avatar")]
        public string Avatar { get; set; }
    }
}
