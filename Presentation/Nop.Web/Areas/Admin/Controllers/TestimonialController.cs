﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Testimonials;
using Nop.Web.Areas.Admin.Extensions;
using Nop.Web.Areas.Admin.Models.Testimonials;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public class TestimonialController : BaseAdminController
    {

        #region Fields
        
        private readonly IPermissionService _permissionService;
        private readonly ITestimonialService _testimonialService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly CustomerSettings _customerSettings;

        #endregion

        #region Ctor

        public TestimonialController(
            IPermissionService permissionService,
            ITestimonialService testimonialService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            CustomerSettings customerSettings)
        {
            this._permissionService = permissionService;
            this._testimonialService = testimonialService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._customerSettings = customerSettings;
        }

        #endregion

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTestimonials))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public virtual IActionResult TestimonialList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTestimonials))
                return AccessDeniedKendoGridJson();

            var testimonial = _testimonialService.GetAllTestimonials();
            var gridModel = new DataSourceResult
            {
                Data = testimonial.Select(t =>
                {
                    var model = t.ToModel();
                    return model;
                }),
                Total = testimonial.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTestimonials))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                _testimonialService.DeleteTestimonials(_testimonialService.GetTestimonialsByIds(selectedIds.ToArray()).ToList());
            }

            return Json(new { Result = true });
        }

        //edit testimonial
        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTestimonials))
                return AccessDeniedView();

            var Testimonial = _testimonialService.GetTestimonialById(id);
            if (Testimonial == null || Testimonial.Deleted)
                //No Testimonial found with the specified id
                return RedirectToAction("List");
            
            var model = Testimonial.ToModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(TestimonialModel model, bool continueEditing, IFormFile uploadedFile)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTestimonials))
                return AccessDeniedView();

            var Testimonial = _testimonialService.GetTestimonialById(model.Id);

            if (Testimonial == null || Testimonial.Deleted)
                //No testimonial found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                if (uploadedFile != null && !string.IsNullOrEmpty(uploadedFile.FileName))
                {
                    var avatarMaxSize = _customerSettings.AvatarMaximumSizeBytes;
                    if (uploadedFile.Length > avatarMaxSize)
                        ErrorNotification(string.Format(_localizationService.GetResource("Account.Avatar.MaximumUploadedFileSize"), avatarMaxSize));
                    model.Avatar = uploadedFile.FileName;
                }
                //Testimonial
                Testimonial = model.ToEntity(Testimonial);
                
                _testimonialService.UpdateTestimonial(Testimonial);

                SuccessNotification(_localizationService.GetResource("admin.contentmanagement.testimonials.updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = Testimonial.Id });
                }
                return RedirectToAction("List");
            }
            
            return View(model);
        }

        //create product
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTestimonials))
                return AccessDeniedView();

            var model = new TestimonialModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(TestimonialModel model, bool continueEditing, IFormFile uploadedFile)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTestimonials))
                return AccessDeniedView();
            
            if (ModelState.IsValid)
            {
                if (uploadedFile != null && !string.IsNullOrEmpty(uploadedFile.FileName))
                {
                    var avatarMaxSize = _customerSettings.AvatarMaximumSizeBytes;
                    if (uploadedFile.Length > avatarMaxSize)
                        throw new NopException(string.Format(_localizationService.GetResource("Account.Avatar.MaximumUploadedFileSize"), avatarMaxSize));
                    model.Avatar = uploadedFile.FileName;
                }
                //Testimonial
                var Testimonial = model.ToEntity();
                Testimonial.CreatedDate = DateTime.Now;
                _testimonialService.InsertTestimonial(Testimonial);

                SuccessNotification(_localizationService.GetResource("admin.contentmanagement.testimonials.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = Testimonial.Id });
                }
                return RedirectToAction("List");
            }
            
            return View(model);
        }

        //delete Testimonial
        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTestimonials))
                return AccessDeniedView();

            var Testimonial = _testimonialService.GetTestimonialById(id);
            if (Testimonial == null)
                //No Testimonial found with the specified id
                return RedirectToAction("List");

            _testimonialService.DeleteTestimonial(Testimonial);

            SuccessNotification(_localizationService.GetResource("admin.contentmanagement.testimonials.Deleted"));
            return RedirectToAction("List");
        }
    }
}