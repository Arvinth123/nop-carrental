﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.FAQ;
using Nop.Services.FAQ;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Extensions;
using Nop.Web.Areas.Admin.Models.FAQ;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public class FAQController : BaseAdminController
    {

        #region Fields

        private readonly IPermissionService _permissionService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IFAQService _FAQService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ILanguageService _languageService;

        #endregion

        #region Ctor

        public FAQController(
            IPermissionService permissionService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IFAQService FAQService,
            ILocalizedEntityService localizedEntityService,
            ILanguageService languageService)
        {
            this._permissionService = permissionService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._FAQService = FAQService;
            this._localizedEntityService = localizedEntityService;
            this._languageService = languageService;
        }

        #endregion

        #region Utilities

        protected virtual void UpdateLocales(FAQuestion FAQ, FAQModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(FAQ,
                    x => x.Question,
                    localized.Question,
                    localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(FAQ,
                    x => x.Answer,
                    localized.Answer,
                    localized.LanguageId);
                
            }
        }

        protected virtual void UpdateLocales(FAQCategory FAQ, FAQCategoryModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(FAQ,
                    x => x.CategoryName,
                    localized.CategoryName,
                    localized.LanguageId);

            }
        }

        #endregion

        public IActionResult Index()
        {
            return View();
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public virtual IActionResult FAQList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedKendoGridJson();

            var FAQuestion = _FAQService.GetAllFAQuestions();
            var gridModel = new DataSourceResult
            {
                Data = FAQuestion.Select(t =>
                {
                    var model = t.ToModel();
                    model.CategoryName = _FAQService.GetFAQCategorynamebyid(model.CategoryId);
                    return model;
                }),
                Total = FAQuestion.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                _FAQService.DeleteFAQuestions(_FAQService.GetFAQuestionByIds(selectedIds.ToArray()).ToList());
            }

            return Json(new { Result = true });
        }

        //edit FAQ
        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            var FAQ = _FAQService.GetFAQuestionById(id);
            if (FAQ == null || FAQ.Deleted)
                //No FAQ found with the specified id
                return RedirectToAction("List");
            var model = FAQ.ToModel();
            var category = _FAQService.GetAllFAQCategorypub();
            foreach (var cat in category)
            {
                model.Categories.Add(new SelectListItem { Text = cat.CategoryName , Value = Convert.ToString(cat.Id) } );
            }
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.Question = FAQ.GetLocalized(x => x.Question, languageId, false, false);
                locale.Answer = FAQ.GetLocalized(x => x.Answer, languageId, false, false);
            });
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(FAQModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            var FAQ = _FAQService.GetFAQuestionById(model.Id);

            if (FAQ == null || FAQ.Deleted)
                //No FAQ found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                //FAQ
                FAQ = model.ToEntity(FAQ);

                _FAQService.UpdateFAQuestion(FAQ);

                //locales
                UpdateLocales(FAQ, model);
                SuccessNotification(_localizationService.GetResource("admin.contentmanagement.FAQ.updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = FAQ.Id });
                }
                return RedirectToAction("List");
            }

            return View(model);
        }


        //create product
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            var model = new FAQModel();

            var category = _FAQService.GetAllFAQCategorypub();
            foreach (var cat in category)
            {
                model.Categories.Add(new SelectListItem { Text = cat.CategoryName, Value = Convert.ToString(cat.Id) });
            }
            AddLocales(_languageService, model.Locales);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(FAQModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                //FAQ
                var FAQ = model.ToEntity();
                FAQ.CreatedDate = DateTime.Now;
                _FAQService.InsertFAQuestion(FAQ);
                //locales
                UpdateLocales(FAQ, model);
                SuccessNotification(_localizationService.GetResource("admin.contentmanagement.FAQ.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = FAQ.Id });
                }
                return RedirectToAction("List");
            }

            return View(model);
        }

        //delete FAQ
        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            var Testimonial = _FAQService.GetFAQuestionById(id);
            if (Testimonial == null)
                //No FAQ found with the specified id
                return RedirectToAction("List");

            _FAQService.DeleteFAQuestion(Testimonial);

            SuccessNotification(_localizationService.GetResource("admin.contentmanagement.FAQ.Deleted"));
            return RedirectToAction("List");
        }

        public virtual IActionResult CategoryList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public virtual IActionResult FAQCategoryList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedKendoGridJson();

            var FAQuestion = _FAQService.GetAllFAQCategory();
            var gridModel = new DataSourceResult
            {
                Data = FAQuestion.Select(t =>
                {
                    var model = t.ToModel();
                    return model;
                }),
                Total = FAQuestion.Count
            };

            return Json(gridModel);
        }


        [HttpPost]
        public virtual IActionResult DeleteSelectedCategory(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                _FAQService.DeleteFAQCategories(_FAQService.GetFAQCategoryByIds(selectedIds.ToArray()).ToList());
            }

            return Json(new { Result = true });
        }

        //edit FAQ
        public virtual IActionResult CategoryEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            var FAQ = _FAQService.GetFAQCategoryById(id);
            if (FAQ == null || FAQ.Deleted)
                //No FAQ found with the specified id
                return RedirectToAction("CategoryList");
            var model = FAQ.ToModel();
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.CategoryName = FAQ.GetLocalized(x => x.CategoryName, languageId, false, false);
            });
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult CategoryEdit(FAQCategoryModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            var FAQ = _FAQService.GetFAQCategoryById(model.Id);

            if (FAQ == null || FAQ.Deleted)
                //No FAQCategory found with the specified id
                return RedirectToAction("CategoryList");

            if (ModelState.IsValid)
            {
                //FAQCategory
                FAQ = model.ToEntity(FAQ);

                _FAQService.UpdateFAQCategory(FAQ);

                //locales
                UpdateLocales(FAQ, model);
                SuccessNotification(_localizationService.GetResource("admin.contentmanagement.FAQCategory.updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("CategoryEdit", new { id = FAQ.Id });
                }
                return RedirectToAction("CategoryList");
            }

            return View(model);
        }

        //create FAQCategory
        public virtual IActionResult CategoryCreate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            var model = new FAQCategoryModel();
            
            AddLocales(_languageService, model.Locales);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult CategoryCreate(FAQCategoryModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                //FAQCategory
                var FAQ = model.ToEntity();
                FAQ.CreatedDate = DateTime.Now;
                _FAQService.InsertFAQCategory(FAQ);
                //locales
                UpdateLocales(FAQ, model);
                SuccessNotification(_localizationService.GetResource("admin.contentmanagement.FAQCategory.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("CategoryEdit", new { id = FAQ.Id });
                }
                return RedirectToAction("CategoryList");
            }

            return View(model);
        }

        //delete FAQCategory
        [HttpPost]
        public virtual IActionResult DeleteCategory(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageFAQ))
                return AccessDeniedView();

            var FAQCategory = _FAQService.GetFAQCategoryById(id);
            if (FAQCategory == null)
                //No FAQ found with the specified id
                return RedirectToAction("CategoryList");

            _FAQService.DeleteFAQCategory(FAQCategory);

            SuccessNotification(_localizationService.GetResource("admin.contentmanagement.FAQCategory.Deleted"));
            return RedirectToAction("CategoryList");
        }

    }
}