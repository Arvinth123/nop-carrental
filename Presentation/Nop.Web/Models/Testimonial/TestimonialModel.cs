﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.Testimonial
{
    public class TestimonialModel : BaseNopModel
    {

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.id")]
        public int? CustomerId { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.name")]
        public string CustomerName { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.rating")]
        public int Rating { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.review")]
        public string Review { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.date")]
        public DateTime CreatedDate { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.published")]
        public bool IsPublished { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.homepage")]
        public bool HomePage { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.testimonialpage")]
        public bool TestimonialPage { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.display")]
        public int? DisplayOrder { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.deleted")]
        public bool Deleted { get; set; }

        [NopResourceDisplayName("admin.contentmanagement.testimonials.fields.avatar")]
        public string Avatar { get; set; }

        public IList<TestimonialModel> Items { get; set; }

    }
}
