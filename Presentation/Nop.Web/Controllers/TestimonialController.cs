﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Services.Localization;
using Nop.Web.Models.Testimonial;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security.Captcha;
using Nop.Core.Domain.Testimonials;
using Nop.Services.Customers;
using Nop.Services.Testimonials;
using Nop.Web.Factories;

namespace Nop.Web.Controllers
{
    public class TestimonialController : Controller
    {

        private readonly IWorkContext _workContext;
        private readonly CatalogSettings _catalogSettings;
        private readonly ILocalizationService _localizationService;
        private readonly CaptchaSettings _captchaSettings;
        private readonly ICustomerService _customerService;
        private readonly ITestimonialService _testimonalService;
        private readonly ITopicModelFactory _topicmodelFactory;

        public  TestimonialController(
            IWorkContext workContext,
            CatalogSettings catalogSettings,
            ILocalizationService localizationService,
            CaptchaSettings captchaSettings,
            ICustomerService customerService,
            ITestimonialService testimonalService,
            ITopicModelFactory topicmodelFactory
            )
        {
            this._workContext = workContext;
            this._catalogSettings = catalogSettings;
            this._localizationService = localizationService;
            this._captchaSettings = captchaSettings;
            this._customerService = customerService;
            this._testimonalService = testimonalService;
            this._topicmodelFactory = topicmodelFactory;
        }

        public IActionResult Testimonial()
        {
            var model = new TestimonialModel();
            //only registered users can leave reviews
            if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
                ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
            return View(model);
        }
        
        [PublicAntiForgery]
        [FormValueRequired("add-review")]
        [ValidateCaptcha]
        public IActionResult Testimonial(TestimonialModel model, bool captchaValid)
        {
            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnProductReviewPage && !captchaValid)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
            }

            if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
            }
            var customer = _customerService.GetCustomerById(_workContext.CurrentCustomer.Id);
            var name = customer.GetFullName();

            if (ModelState.IsValid)
            {
                var Testimonial = new Testimonial
                {
                    CustomerId = _workContext.CurrentCustomer.Id,
                    CustomerName = name,
                    Review = model.Review,
                    Rating = model.Rating,
                    CreatedDate = DateTime.Now,
                    IsPublished = false,
                    HomePage = false,
                    TestimonialPage = false,
                    DisplayOrder = 0,
                    Deleted = false,
                };
            _testimonalService.InsertTestimonial(Testimonial);
            }

            return RedirectToAction("TestimonialList", "Testimonial");
        }

        public IActionResult TestimonialList()
        {
            var model = new TestimonialModel();
            //only registered users can leave reviews
            if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
                ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
            model.Items = _topicmodelFactory.GetAllTestimonialPage();
            return View(model);
        }
    }
}